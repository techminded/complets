
import { ResLoader } from "../res-loader.js";


export class XmlHttpClient {

    //auth;


    request(url, type = 'GET', contentType, body) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(type, url);
            if (contentType !== 'auto') {
                xhr.setRequestHeader("Content-Type", contentType ? contentType : 'application/json');
            }
            if (this.auth && this.auth.type === 'basic') {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(this.auth.username + ':' + this.auth.password));
            }
            if (! ResLoader.isInIE()) {
                xhr.responseType = 'json';
            }
            xhr.onreadystatechange = (event) => {
                let request = event.target;
                if (request.readyState === XMLHttpRequest.DONE) {
                    if (request.status === 200) {
                        resolve(request);
                    } else {
                        reject(request);
                    }
                }
            };
            if (body) {
                xhr.send(body);
            } else {
                xhr.send();
            }
        });
    }

    get(url, contentType) {
        return this.request(url, 'GET', contentType ? contentType : null, null);
    }

    post(url, contentType, body) {
        return this.request(url, 'POST', contentType ? contentType : null, body);
    }
}