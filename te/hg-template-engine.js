

export class HgTemplateEngine {


    constructor(handlebars) {
        this.handlebars = handlebars;
    }

    renderString(tplString, data) {
        let templateFunc = this.handlebars.compile(tplString);
        let result = templateFunc(data);
        return result;
    }
    render(tpl, data) {
        if (typeof tpl === 'object') {
            let wrapper = document.createElement('div');
            wrapper.appendChild(tpl);
            let templateString = wrapper.outerHTML.toString();
            let rendered = this.renderString(templateString, data);
            let wrapper2 = document.createElement('div');
            wrapper2.insertAdjacentHTML('beforeend', rendered);
            return wrapper2.firstElementChild.innerHTML;
        } else {
            return this.renderString(tpl, data);
        }
    }
}