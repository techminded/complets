

export class BaseTemplateEngine {

    renderMustacheVars(el, map) {
        let wrapper = document.createElement('div');
        wrapper.appendChild(el);
        let template = wrapper.outerHTML.toString();

        for (let key of Object.keys(map)) {
            template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }
        let wrapper2 = document.createElement('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
    }

    renderString(tplString, data) {
        let resultString = (' ' + tplString).slice(1);
        for (let key of Object.keys(data)) {
            resultString = resultString.replace(new RegExp('{{ ' + key + ' }}', 'g'), data[key]);
        }
        return resultString;
    }

    render(tpl, data) {
        if (typeof tpl === 'object') {
            return this.renderMustacheVars(tpl, data);
        } else {
            return this.renderString(tpl, data);
        }
    }
}