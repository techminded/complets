


export class ResLoader {

    static dynamicImportSupported() {
        try {
            new Function('import("")');
            return true;
        } catch (err) {
            return false;
        }
    }

    static isInIE() {
        let ua = window.navigator.userAgent;

        let msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        let trident = ua.indexOf('Trident/');
        if (trident > 0) {
            let rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        let edge = ua.indexOf('Edge/');
        if (edge > 0) {
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return false;
    }


    static loadClass(className, path, callback, nowindow = false) {
        let loaded;
        let setToWin = () => {
            if (! nowindow) {
                loaded.then(function(m) {
                    window[className] = m[className];
                    return m;
                });
            }
        };
        let applyCallback = () => {
            if (callback) {
                loaded.then(callback);
            }
        };
        if (ResLoader.dynamicImportSupported() && ! ResLoader.isInIE()) {
            loaded = import(path);
            setToWin();
            applyCallback();
            return loaded;
        } else {
            loaded = new Promise(function(resolve, reject) {
                let script = document.createElement('script');
                script.onload = resolve;
                script.onerror = reject;
                script.async = true;
                script.src = path;
                document.body.appendChild(script);
            }.bind(this));
            setToWin();
            applyCallback();
            return loaded;
        }
    }
}