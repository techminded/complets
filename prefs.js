

export function loadFromUrl(url) {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.setRequestHeader("Content-Type", 'application/json');
        xhr.onreadystatechange = (event) => {
            if(event.target.readyState == XMLHttpRequest.DONE) {
                if (event.target.status == 200) {
                    let settings = JSON.parse(event.target.response);
                    resolve(settings);
                } else {
                    reject(event);
                }
            }
        };

        xhr.send();
    });

}
